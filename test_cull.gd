@tool
extends Node3D

## instances_cull_aabb issues
##
## See sweep_probe() for the meat

@export var start:bool:
	set(b):
		print("\n******** START *********")
		RenderingServer.force_sync() # Wild guess? Does not help.
		start = false
		probe.position = startpos
		intersecting=false
		set_process(true)

@export var stop:bool:
	set(b):
		stop = false
		probe.position = startpos
		set_process(false)

@onready var probe:=$probe

var startpos:=Vector3(0, 1.5, 0.5)
var intersecting:=false

func _process(delta: float) -> void:
	probe.position.y -= delta * 4
	sweep_probe()

func sweep_probe():
	var aabb:AABB = probe.mesh.get_aabb()
	#aabb.position = probe.global_position # does not work at all
	aabb.position = (probe.global_transform * aabb).position # seems ok

	# Verify probe and the aabb are kind of in the same place:
	# print("\nprobe at:", probe.position, " vs: aabb at :", aabb.get_center())
	assert(probe.position == aabb.get_center())

	# Weirdness:
	# instances_cull_aabb immediately gets the last mesh I duplicated in the editor.
	# Is there any way to force an update of that so pa is empty when there's no
	# actual intersection?
	# Even a restart of Godot does not change this. It always has mesh3, for e.g.
	probe.visible = false
	var pa:PackedInt64Array = RenderingServer.instances_cull_aabb(aabb, get_world_3d().scenario)
	probe.visible = true
	if pa:
		print("   HIT :")
		for objid in pa:
			printraw("   ",instance_from_id(objid).name, ",")

		# Just force it to stop for now to show that something was
		# hit even though nothing should have been.
		stop = true
