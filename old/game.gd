extends Node3D

## Area intersection isue
##
## Trying to make an Aread3D by using nodes and
## test for intersection with a box shape.
##
## Open the scene and click the bool Tog in the inspector
## Observe the printed output.
##
## I expect to see:
## *****************************
## one
## two
## three
## TEST HIT
## Bingo!

var A:Area3D
var state:PhysicsDirectSpaceState3D

func _on_button_pressed() -> void:
	await set_state()
	await wtf()

func set_state():
	state = null
	await get_tree().physics_frame
	if not state:
		state = get_world_3d().direct_space_state

func wtf():
	await create_area3d_node()
	await node_probe()

func create_area3d_node():
	print()
	print("*****************************")

	var _a = find_child("AREANODE")
	if _a:
		remove_child(_a)
		_a.queue_free()
		A = null

	A = Area3D.new()
	A.name = "AREANODE"
	add_child(A)
	A.set_owner(self.owner)

	await get_tree().physics_frame


func node_probe():
	var box:BoxShape3D
	# make a shape, the same size as b
	var poke = [$one, $two, $three]
	for b in poke:
		print(b.name)
		box = BoxShape3D.new()
		box.size = b.mesh.size
		var coll:CollisionShape3D
		coll = CollisionShape3D.new()
		coll.name = b.name
		coll.transform = b.transform
		coll.shape = box
		A.add_child(coll)
		coll.set_owner(A.owner)
		await get_tree().physics_frame

	#As per docs
	var params:PhysicsShapeQueryParameters3D = PhysicsShapeQueryParameters3D.new()
	var shape_rid = PhysicsServer3D.box_shape_create()
	PhysicsServer3D.shape_set_data(shape_rid, $overlapper.mesh.size)
	params.shape_rid = shape_rid
	params.transform = $overlapper.transform
	params.collide_with_areas = true
	params.collide_with_bodies = false

	print("TEST HIT")
	var surrounds = state.intersect_shape(params, 6)

	PhysicsServer3D.free_rid(shape_rid)

	if surrounds:
		print("Bingo!")





func xnode_probe():
	var box:BoxShape3D
	# make a shape, the same size as b
	var poke = [$one, $two, $three]
	for b in poke:
		print(b.name)
		box = BoxShape3D.new()
		box.size = b.mesh.size
		var coll:CollisionShape3D
		coll = CollisionShape3D.new()
		coll.name = b.name
		coll.transform = b.transform
		coll.shape = box
		A.add_child(coll)
		coll.set_owner(A.owner)
		await get_tree().physics_frame

	#As per docs
	var params:PhysicsShapeQueryParameters3D = PhysicsShapeQueryParameters3D.new()
	var shape_rid = PhysicsServer3D.box_shape_create()
	PhysicsServer3D.shape_set_data(shape_rid, $overlapper.mesh.size)
	params.shape_rid = shape_rid
	params.transform = $overlapper.transform
	params.collide_with_areas = true
	params.collide_with_bodies = false

	print("TEST HIT")
	var surrounds = state.intersect_shape(params, 6)

	PhysicsServer3D.free_rid(shape_rid)

	if surrounds:
		print("Bingo!")
