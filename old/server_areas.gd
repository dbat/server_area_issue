@tool
extends Node3D

## Area intersection isue
##
## Trying to make an area by PhysicsServer3D and
## test for intersection with a box shape
##
## Open the scene and click the bool Togs in the inspector
## Observe the printed output.

var Area_RID:RID
var A:Area3D
var state:PhysicsDirectSpaceState3D


@export var WTF:bool:
	set(b):
		WTF = false
		await set_state()
		await wtf()

@export var WORKS:bool:
	set(b):
		WORKS = false
		await set_state()
		await works()

@export var FAILS:bool:
	set(b):
		FAILS = false
		await set_state()
		await fails()

func set_state():
	state = null
	await get_tree().physics_frame
	if not state:
		state = get_world_3d().direct_space_state
		print("STATE set to:", state)

## Uses nodes to make the Area3D
## but does the rest with PhysicsServer3D
func works():
	await create_area3d_node()
	await no_node_probe()

## Tries to make the Area3D with PhysicsServer3D. No dice.
func fails():
	await create_area_via_server()
	await no_node_probe()

func wtf():
	await create_area3d_node()
	await node_probe()

func create_area3d_node():
	print()
	print("*****************************")

	var _a = find_child("AREANODE")
	if _a:
		remove_child(_a)
		_a.queue_free()
		A = null

	A = Area3D.new()
	A.name = "AREANODE"
	add_child(A)
	A.set_owner(self.owner)
	Area_RID = A.get_rid()

	await get_tree().physics_frame


func create_area_via_server():
	print()
	print("*****************************")
	if Area_RID:
		PhysicsServer3D.area_clear_shapes(Area_RID)
		print("Just cleared area, count :", PhysicsServer3D.area_get_shape_count(Area_RID))
		#PhysicsServer3D.free_rid(Area_RID)
		return

	# Make the area
	Area_RID = PhysicsServer3D.area_create()
	#var state_rid = PhysicsServer3D.space_create()
	PhysicsServer3D.area_set_space(Area_RID, state)
	PhysicsServer3D.area_set_transform(Area_RID, global_transform)
	# Debug prints
	var areas_transform = PhysicsServer3D.area_get_transform(Area_RID)
	print("area's transform:", areas_transform)
	print("mine            :", transform)

	# Just wildly stabbing in the dark. Trying to get
	# this server-area to work at all.
	PhysicsServer3D.area_attach_object_instance_id(Area_RID, self.get_instance_id())
	PhysicsServer3D.area_set_collision_layer(Area_RID, 1)
	PhysicsServer3D.area_set_collision_mask(Area_RID, 1)
	#Seems to make no difference
	PhysicsServer3D.area_set_monitorable(Area_RID, true)
	# Also makes no diff
	PhysicsServer3D.area_set_ray_pickable(Area_RID, true)
	# Looked promising, but hey - makes no difference!
	PhysicsServer3D.set_active(true)

	print("Tried to make an area via server")
	await get_tree().physics_frame


## Does the usual intersect_shape and then uses server
## for all the rest.
func no_node_probe():
	var box:BoxShape3D
	# I have four meshes so that the last one overlaps the
	# first three - these are used as guides in this loop:
	for b in get_children():
		if not b is MeshInstance3D: continue
		print(b.name) #, " at ", b.transform)
		# make a shape, the same size as b
		var shape_rid
		#PhysicsServer3D.set_active(true)
		shape_rid = PhysicsServer3D.box_shape_create()
		PhysicsServer3D.shape_set_data(shape_rid, b.mesh.size)

		#As per docs
		var params:PhysicsShapeQueryParameters3D = PhysicsShapeQueryParameters3D.new()
		params.shape_rid = shape_rid
		params.transform = b.transform #global_transform
		params.collide_with_bodies = false
		params.collide_with_areas = true

		var surrounds = state.intersect_shape(params,1)

		# I only care about any hit at all.
		if surrounds:
			print("  We got a hit!! ", surrounds)
			break

		var newt:Transform3D = params.transform
		# Weirdly, this call does seem to work, only when the
		# Area_RID is from an actual Area3D node.
		PhysicsServer3D.area_add_shape(Area_RID, shape_rid, newt)
		print(" shape count :", PhysicsServer3D.area_get_shape_count(Area_RID))

		await get_tree().physics_frame # Seems neccessary.

	print(" final shape count :", PhysicsServer3D.area_get_shape_count(Area_RID))





# Not used as no_node_probe seems to work.
func node_probe():
	var box:BoxShape3D
	# make a shape, the same size as b
	var poke = [$one, $two, $three]
	for b in poke:
		box = BoxShape3D.new()
		box.size = b.mesh.size
		var newt:Transform3D = b.transform
		var coll:CollisionShape3D
		coll = CollisionShape3D.new()
		coll.name = "booger%s" % randi()
		coll.transform = b.transform
		coll.shape = box
		A.add_child(coll)
		coll.set_owner(A.owner)
		print(" added collider at:", newt)
		await get_tree().physics_frame

	#As per docs
	var params:PhysicsShapeQueryParameters3D = PhysicsShapeQueryParameters3D.new()
	#box = BoxShape3D.new()
	#box.size = $overlapper.mesh.size
	#params.shape = box
	var shape_rid
	shape_rid = PhysicsServer3D.box_shape_create()
	PhysicsServer3D.shape_set_data(shape_rid, $overlapper.mesh.size)
	params.shape_rid = box.get_rid()
	params.transform = $overlapper.transform
	params.collide_with_bodies = false
	params.collide_with_areas = true # <-- surely?

	# Execute physics queries here...
	print("TEST HIT")
	var surrounds = state.intersect_shape(params, 6)

	PhysicsServer3D.free_rid(shape_rid)

	if surrounds:
		print("Bingo!")
	else:
		print("nope")







#
## Not used as no_node_probe seems to work.
#func node_probe():
	#var box:BoxShape3D
	## make a shape, the same size as b
	#var shape_rid
	#for b in get_children():
		#if not b is MeshInstance3D: continue
		#box = BoxShape3D.new()
		#box.size = b.mesh.size
		#shape_rid = box.get_rid()
#
		##As per docs
		#var params:PhysicsShapeQueryParameters3D = PhysicsShapeQueryParameters3D.new()
		#params.shape_rid = shape_rid
		#params.transform = b.transform
		#params.collide_with_bodies = false
		#params.collide_with_areas = true # <-- surely?
#
		## Execute physics queries here...
		#var space_rid = PhysicsServer3D.area_get_space(Area_RID)
		#state = PhysicsServer3D.space_get_direct_state(space_rid)
		##state = get_world_3d().direct_space_state
		#PhysicsServer3D.set_active(true)
		#var surrounds = state.intersect_shape(params,6)
#
		##print(A.get_overlapping_areas())
#
		#print(str(surrounds).replace("}, {", "\n"))
#
		## If we did not hit anything, place a shape into the area
		## for the next round
		#if surrounds.is_empty():
			#var newt:Transform3D = params.transform
			#var coll:CollisionShape3D
			#coll = CollisionShape3D.new()
			#coll.name = "booger%s" % randi()
			#coll.transform = b.transform
			#coll.shape = box
			#A.add_child(coll)
			#coll.set_owner(A.owner)
			#print(" added collider/shape at:", newt)
#
		#await get_tree().process_frame
