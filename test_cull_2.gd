@tool
extends Node3D

## instances_cull_aabb issues
##
## If you move any of them 'mesh' nodes in the editor, and then
## hit start, the detection fails because that node is in some
## kind of cache.
##
## If you hide/show 'test' node then hit start again it will work.
## That somehow removes that moved node from a cache.

@export var start:bool:
	set(b):
		print("\n******** START *********")
		#place_targets() # uncomment to get code placed targets
		start = false
		probe.position = startpos
		intersecting=false
		set_process(true)

@export var stop:bool:
	set(b):
		stop = false
		probe.position = startpos
		set_process(false)
		#remove_targets() # uncomment to clear the targets

@onready var probe:=$probe

var startpos:=Vector3(0, 1.5, 0.5)
var intersecting:=false

var pos = [Vector3(0,0,0),Vector3(0,0,1.4),Vector3(1,0,0.4)]

func _ready() -> void:
	self.set_process(false)

func place_targets():
	for t in pos:
		var m = MeshInstance3D.new()
		m.mesh = BoxMesh.new()
		m.name = "target_%s" % randi()
		m.position = t
		add_child(m)
		m.set_owner(self.owner)
		print(m.name)
		await get_tree().process_frame

func remove_targets():
	var ts = find_children("target_*", "", false, true)
	for t in ts:
		remove_child(t)
		t.queue_free()

func _process(delta: float) -> void:
	probe.position.y -= delta * 1
	sweep_probe()

func sweep_probe():
	var aabb:AABB = probe.mesh.get_aabb()
	#aabb.position = probe.global_position # does not work at all
	aabb.position = (probe.global_transform * aabb).position # seems ok

	# Verify probe and the aabb are kind of in the same place:
	var msg = "probe: %v vas aabb: %v" % [probe.position,aabb.get_center()]
	assert(probe.position.is_equal_approx(aabb.get_center()), msg)

	probe.visible = false
	var pa:PackedInt64Array = RenderingServer.instances_cull_aabb(aabb, get_world_3d().scenario)
	probe.visible = true
	if pa:
		printraw("   HIT :")
		for objid in pa:
			printraw(instance_from_id(objid).name, ",")
		print()
	if probe.position.y < -1.5:
		print("STOP")
		stop=true
